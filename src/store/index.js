import Vue from 'vue'
import Vuex from 'vuex'
import topStories from './modules/topStories';
import item from './modules/item';
import auth from './modules/auth';
import hnya from './modules/hnya';
import backpacks from './modules/backpacks';

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    backpacks,
    hnya,
    auth,
    item,
    topStories
  }
})