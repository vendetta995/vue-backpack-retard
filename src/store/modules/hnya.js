// import axios from "axios";
import * as fb from '../../firebase';

export default {
  namespaced: true,
  state: {
    liked: [],
    reposted: []
  },
  getters: {
    userLikedItemsIds(state) {
      return state.liked.map(i => i.itemId);
    },
    userRepostedItemsIds(state) {
      return state.reposted.map(i => i.itemId);
    },
    userLikedItems(state) {
      return state.liked;
    },
    userRepostedItems(state) {
      return state.reposted;
    }
  },
  actions: {
    async unlikeItem(ctx, itemId) {
      const likes = await fb.likesCollection.where("itemId", "==", itemId).get();
      await Promise.all(likes.docs.map(d=>d.ref.delete()));
      ctx.dispatch('fetchLikedItems');
    },
    async unrepostItem(ctx, itemId) {
      const reposts = await fb.repostCollection.where("itemId", "==", itemId).get();
      await Promise.all(reposts.docs.map(d=>d.ref.delete()));
      ctx.dispatch('fetchRepostedItems');
    },
    async likeItem(ctx, { itemId, type, title = '', text = '' }) {
      await fb.likesCollection.add({
        userId: fb.auth.currentUser.uid,
        itemId, type, title, text
      })
      ctx.dispatch('fetchLikedItems');
    },
    async repostItem(ctx, { itemId, type, title = '', text = '' }) {
      await fb.repostCollection.add({
        userId: fb.auth.currentUser.uid,
        itemId, type, title, text
      })
      ctx.dispatch('fetchRepostedItems');
    },
    async fetchLikedItems(ctx) {
      const liked = await fb.likesCollection.where("userId", "==", fb.auth.currentUser.uid).get();
      ctx.commit('updateLiked', liked);
    },
    async fetchRepostedItems(ctx) {
      const reposts = await fb.repostCollection.where("userId", "==", fb.auth.currentUser.uid).get();
      ctx.commit('updateReposted', reposts);
    },
  },
  mutations: {
    updateLiked(state, liked) {
      state.liked = liked.docs.map(l => l.data());
    },
    updateReposted(state, reposted) {
      state.reposted = reposted.docs.map(l => l.data());
    }
  }
}