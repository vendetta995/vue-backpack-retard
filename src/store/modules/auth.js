// import axios from "axios";
import * as fb from '../../firebase';
import router from '../../router';
import ratingCalculator from '@/utils/ratingCalculator';
import userRatingCalculator from '@/utils/userRatingCalculator';
export default {
  namespaced: true,
  state: {
    userProfile: {},
  },
  getters: {
    userProfile(state) {
      return state.userProfile;
    }
  },
  actions: {
    async logoutUser(ctx) {
      await fb.auth.signOut();
      ctx.commit('setUserProfile', {});
      router.push('/');
    },
    async signup(ctx, form) {
      const { user } = await fb.auth.createUserWithEmailAndPassword(form.email, form.password)
      await user.updateProfile({
        displayName: form.name,
        title123:form.title
      })
      const ref = await fb.usersCollection.doc(user.uid)
      await ref.set({
        email: form.email,
        name: form.name,
        title: form.title
      })

      await ctx.dispatch('fetchUserProfile', user)
      router.push('/');
    },
    async login(ctx, form) {
      await fb.firebase.auth().setPersistence(fb.firebase.auth.Auth.Persistence.SESSION)
      const { user } = await fb.auth.signInWithEmailAndPassword(form.email, form.password)
      try {
        await ctx.dispatch('fetchUserProfile', user);
        router.replace(router.currentRoute.query.from || '/');
      } catch (err) {
        console.log(err)
      }
    },
    async fetchUserProfile(ctx, user) {
      if (user) {
        const userProfile = await fb.usersCollection.doc(user.uid).get();
        await ctx.dispatch('hnya/fetchLikedItems', null, { root: true })
        await ctx.dispatch('hnya/fetchRepostedItems', null, { root: true })
        await ctx.dispatch('backpacks/fetchUsersBackpacks', user.uid, { root: true })
        const usersBackpacks = ctx.rootGetters['backpacks/usersBackpacks'];
        const usersRating = userRatingCalculator(usersBackpacks.map(bp => ratingCalculator(bp.ratings)));
        ctx.commit('setUserProfile', Object.assign({}, { id: user.uid, rating: usersRating }, { ...userProfile.data() }))
      } else {
        ctx.commit('setUserProfile', {})
      }
    }
  },
  mutations: {
    setUserProfile(state, val) {
      state.userProfile = val
    }
  }
}