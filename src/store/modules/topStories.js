const getItemLink = (id) =>
  `https://hacker-news.firebaseio.com/v0/item/${id}.json?print=pretty`;
import axios from "axios";

export default {
  actions: {
    async fetchTopStories(ctx) {
      const data = await axios.get(
        "https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty"
      ).then((response) => {
        return Promise.all(
          response.data.slice(0, 10).map((id) => axios.get(getItemLink(id)))
        );
      })
        .then((response) => {
          return response.map((item) => item.data)
        });
      ctx.commit('updateTopStories', data);
    }
  },
  mutations: {
    updateTopStories(state, topStories) {
      state.topStories = topStories;
    }
  },
  state: {
    topStories: []
  },
  getters: {
    allTopStories(state){
      return state.topStories;
    }
  }  
}