const getItemLink = (id) =>
  `https://hacker-news.firebaseio.com/v0/item/${id}.json?print=pretty`;
import axios from "axios";
import router from '../../router';
export default {
  state: {
    items: {},
  },
  actions: {
    async goToParentPost(ctx, itemId) {
      await ctx.dispatch("fetchItem", itemId);
      let item = ctx.getters.getItemById(itemId);
      while (item.parent) {
        await ctx.dispatch("fetchItem", item.parent);
        item = ctx.getters.getItemById(item.parent);
      }
      router.push(`/story/${item.id}`);
    },
    async fetchKids(ctx, { kidsIds }) {
      const kids = await Promise.all((kidsIds || []).map(async id => {
        const res = await axios.get(getItemLink(id));
        return res.data;
      }));
      kids.forEach((item) => {
        ctx.commit('addFetchedItemToState', item);
        ctx.commit('changeStatus', { id: item.id, status: 'success' });
      });
    },
    async fetchStory(ctx, storyId) {
      await ctx.dispatch("fetchItem", storyId);
      const story = ctx.getters.getItemById(storyId);
      if (story) {
        await ctx.dispatch("fetchKids", { itemId: story.id, kidsIds: story.kids });
        ctx.commit("applyCommentsToStory", { commentIds: story.kids, id: story.id });
      } else {
        const story = ctx.getters.getItemById(storyId);
        await ctx.dispatch('fetchItem', storyId);
        await ctx.dispatch("fetchKids", { itemId: story.id, kidsIds: story.kids });
        ctx.commit("applyCommentsToStory", { commentIds: story.kids, id: story.id });
      }
    },
    async fetchItem(ctx, itemId) {
      const basicItem = { id: itemId, status: 'loading' };
      ctx.commit('addFetchedItemToState', basicItem);
      ctx.commit('changeStatus', { id: itemId, status: 'loading' });
      const item = await axios.get(getItemLink(itemId)).then(res => res.data);
      ctx.commit('addFetchedItemToState', item);
      ctx.commit('changeStatus', { id: itemId, status: 'success' });
    }
  },
  getters: {
    itemStatus: (state) => (itemId) => {
      return state.items[itemId] ? state.items[itemId].status : 'loading';
    },
    getItemById: (state) => (itemId) => {
      return state.items[itemId] ? state.items[itemId] : null;
    }
  },
  mutations: {
    applyCommentsToStory(state, { commentIds, id }) {
      const comments = (commentIds || []).map(commentId => {
        return state.items[commentId] ? state.items[commentId] : {};
      });
      const updatedItem = {
        [id]: Object.assign({}, state.items[id], { comments })
      };
      state.items = { ...state.items, ...updatedItem };
    },
    addFetchedItemToState(state, item) {
      const newItem = {
        [item.id]: item
      };
      state.items = { ...state.items, ...newItem };
    },
    changeStatus(state, { id, status }) {
      const updatedItem = {
        [id]: Object.assign({}, state.items[id], { status })
      };
      state.items = { ...state.items, ...updatedItem };
    }
  },
}