import * as fb from '../../firebase';
import faker from 'faker';
import router from '../../router';
import { v4 as uuidv4 } from "uuid";
import { auth } from '../../firebase';
export default {
  namespaced: true,
  state: {
    allBackpacks: [],
    currentBackpack: null,
    usersBackpacks: []
  },
  getters: {
    allBackpacks(state) {
      return state.allBackpacks;
    },
    currentBackpack(state) {
      return state.currentBackpack;
    },
    usersBackpacks(state) {
      return state.usersBackpacks;
    }
  },
  actions: {
    async updateCurrentBackpack(ctx) {
      const bp = ctx.getters.currentBackpack;
      await fb.backpacksCollection.doc(bp.id).set(bp);
    },
    async rateBackpack(ctx, info) {
      ctx.commit('addRating', { userId: auth.currentUser.uid, rate: info.rate });
      await ctx.dispatch('updateCurrentBackpack');
    },
    async addNewBackpack(ctx, backpackName) {
      const user = ctx.rootGetters['auth/userProfile'];
      const newBp = {
        title: backpackName,
        creatorId: user.id || 1,
        creatorName: user.name || 'Sample name',
        items: [],
        description: faker.random.words()
      };
      try {
        const backpack = await fb.backpacksCollection.add(newBp);
        ctx.commit('addNewBackpackToBackpacks', Object.assign({}, { id: backpack.id }, { ...newBp }));
        router.push(`/backpack/${backpack.id}`);
        // ctx.dispatch('fetchUsersBackpacks', user.id)
      } catch (err) {
        console.log(err)
      }
    },
    async fetchAllBackpacks(ctx) {
      const all = await fb.backpacksCollection.get();
      ctx.commit('setAllBackpacks', all);
    },
    async fetchUsersBackpacks(ctx, userId) {
      const all = await fb.backpacksCollection.where('creatorId', '==', userId).get();
      ctx.commit('setUsersBackpacks', all);
    },
    async fetchBackpack(ctx, backpackId) {
      const bp = await fb.db.doc(`backpacks/${backpackId}`).get();
      ctx.commit('setCurrentBackpack', bp)
    },
    // async deleteBackpack(ctx,backpackId)
    // async updateBackpack(ctx,backpack)
    // async createBackpack(ctx,info){}
  },
  mutations: {
    addRating(state, rate) {

      const newRatings = { ...state.currentBackpack.ratings, [rate.userId]: rate.rate };
      state.currentBackpack = Object.assign({}, { ...state.currentBackpack }, { ratings: newRatings });
    },
    removeRating(state, rate) {
      const newRatings = { ...state.currentBackpack.ratings, [rate.userId]: 0 };
      state.currentBackpack = Object.assign({}, { ...state.currentBackpack }, { ratings: newRatings });
    },
    setTitle(state, title) {
      state.currentBackpack = Object.assign({}, { ...state.currentBackpack }, { title })
    },
    setOccasion(state, occasion) {
      state.currentBackpack = Object.assign({}, { ...state.currentBackpack }, { occasion })
    },
    setDescription(state, description) {
      state.currentBackpack = Object.assign({}, { ...state.currentBackpack }, { description })
    },
    toggleItemInCurrentBackpack(state, itemId) {
      const updatedItems = state.currentBackpack.items.map((item) => {
        return item.id === itemId
          ? Object.assign({}, item, { crossed: !item.crossed })
          : item;
      });
      state.currentBackpack = Object.assign({}, { ...state.currentBackpack }, { items: updatedItems })
    },
    addItemForCurrentBackpack(state, item) {
      const updatedItems = [...state.currentBackpack.items, { id: uuidv4(), crossed: false, title: item.itemName }]
      state.currentBackpack = Object.assign({}, { ...state.currentBackpack }, { items: updatedItems })
    },
    deleteItemFromCurrentBackpack(state, itemId) {
      const updatedItems = state.currentBackpack.items.filter((item) => item.id !== itemId);
      state.currentBackpack = Object.assign({}, { ...state.currentBackpack }, { items: updatedItems })
    },
    setUsersBackpacks(state, all) {
      state.usersBackpacks = all.docs.map(l => ({ id: l.id, ...l.data() }));
    },
    addNewBackpackToBackpacks(state, backpack) {
      state.allBackpacks = [backpack, ...state.allBackpacks];
    },
    setAllBackpacks(state, all) {
      state.allBackpacks = all.docs.map(l => ({ id: l.id, ...l.data() }));
    },
    setCurrentBackpack(state, bp) {
      state.currentBackpack = Object.assign({}, { id: bp.id }, { ...bp.data() });
    }
  }
}