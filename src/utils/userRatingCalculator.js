module.exports = function (ratings) {
  const droppedZeroes = ratings.filter(rating=>rating)
  const summarizer = (accumulator, currentValue) =>
    accumulator + currentValue;
  const marks = Object.values(droppedZeroes || {});
  if (marks.length) {
    return Math.round((marks.reduce(summarizer) / marks.length) * 10) / 10;
  }
  return 5;
}