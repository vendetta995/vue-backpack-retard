module.exports = function (ratings) {
  const summarizer = (accumulator, currentValue) =>
    accumulator + currentValue;
  const marks = Object.values(ratings || {});
  if (marks.length) {
    return Math.round((marks.reduce(summarizer) / marks.length) * 10) / 10;
  }
  return 0;
}