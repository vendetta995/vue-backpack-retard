import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/pages/HomePage';
// import StoryPage from '@/pages/StoryPage';
import LoginPage from '@/pages/LoginPage';
import Backpack from '@/pages/BackpackPage';
import AllBackpacks from '@/pages/AllBackpacksPage';
import MyBackpacks from '@/pages/MyBackpacksPage';
import { auth } from './firebase';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Home
    },
    {
      path: '/login',
      name: 'login',
      component: LoginPage
    },
    {
      path: '/all-backpacks',
      component: AllBackpacks,
    },
    {
      path: '/my-backpacks',
      component: MyBackpacks,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/backpack/:id',
      name: 'backpack',
      component: Backpack,
      meta: {
        requiresAuth: true
      }
    },
    // {
    //   path: '/hnya',
    //   component: () => import('@/pages/HnyaPage'),
    //   children: [
    //     {
    //       path: 'likes',
    //       component: ()=> import('@/pages/HnyaPage/Likes'), meta: {
    //         requiresAuth: true
    //       }
    //     },
    //     {
    //       path: 'reposts',
    //       component: ()=> import('@/pages/HnyaPage/Reposts'), meta: {
    //         requiresAuth: true
    //       }
    //     }
    //   ],
    //   meta: {
    //     requiresAuth: true
    //   }
    // },
    // {
    //   path: '/stories',
    //   component: () => import('@/pages/PostsPage'),
    //   meta: {
    //     requiresAuth: true
    //   }
    // }, {
    //   path: '/story/:id',
    //   name: 'story',
    //   component: StoryPage,
    //   meta: {
    //     requiresAuth: true
    //   }
    // },
  ]
})

router.beforeEach(async (to, from, next) => {
  const requiresAuth = to.matched.some(x => x.meta.requiresAuth)
  if (requiresAuth && !auth.currentUser) {
    const loginpath = to.path;
    next({ name: 'login', query: { from: loginpath } });
  } else {
    next()
  }
})
export default router;