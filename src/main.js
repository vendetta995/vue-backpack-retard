import Vue from 'vue'
import App from './App.vue'
import store from './store';
import router from './router';
import vmodal from 'vue-js-modal'
import { auth } from './firebase'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faStar, faChevronRight } from '@fortawesome/free-solid-svg-icons'
import { faStar as farStar } from '@fortawesome/free-regular-svg-icons'
import AOS from 'aos'
import 'aos/dist/aos.css'

library.add(faStar)
library.add(farStar)
library.add(faChevronRight)
Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false
Vue.use(vmodal);
let app
auth.onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      router,
      store,
      async created () {
        AOS.init();
        await store.dispatch('auth/fetchUserProfile', auth.currentUser)
      },
      render: h => h(App),
    }).$mount('#app')
  }
})
