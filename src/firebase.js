import { firebase } from '@firebase/app'
import '@firebase/auth'
import '@firebase/firestore'

// firebase init - add your own config here
var firebaseConfig = {
  apiKey: "AIzaSyDDRW7ynUohnyhVJShATMw7EYc16nIlqUU",
  authDomain: "my-hackernews-dae87.firebaseapp.com",
  databaseURL: "https://my-hackernews-dae87.firebaseio.com",
  projectId: "my-hackernews-dae87",
  storageBucket: "my-hackernews-dae87.appspot.com",
  messagingSenderId: "673238459185",
  appId: "1:673238459185:web:8c63f6ca8fd69f2b768610"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

// utils
const db = firebase.firestore()
const auth = firebase.auth()

// collection references
const usersCollection = db.collection('users')
const postsCollection = db.collection('posts')
const repostCollection = db.collection('reposts')
const commentsCollection = db.collection('comments')
const likesCollection = db.collection('likes')
const backpacksCollection = db.collection('backpacks')

// export utils/refs
export {
  firebase,
  db,
  auth,
  usersCollection,
  postsCollection,
  repostCollection,
  commentsCollection,
  likesCollection,
  backpacksCollection
}