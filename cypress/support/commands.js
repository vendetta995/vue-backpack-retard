// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
Cypress.Commands.add("login", (email, password) => {
  cy.visit('/')
  cy.get('[data-test-login-link]').click();
  cy.url({ timeout: 10000 }).should('contains', `/login`);
  cy.get('[data-test-login-email-input]').type(email)
  cy.get('[data-test-login-password-input]').type(password)
  cy.get('[data-test-login-button]').click()
  cy.url({ timeout: 10000 }).should('eq', `${Cypress.config().baseUrl}/`);
})
Cypress.Commands.add('clearSessionStorage', () => {
  cy.window().then((win) => {
    win.sessionStorage.clear()
  });
})
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
