# vue2

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

# Live environment

You can see project live on https://vue-backpack-retard.netlify.app using
test credentials: daniel@test.com 123456

## Tests
- Unit tests are powered by vue-test-utils
- e2e tests are powered by cypress
  
## CI 

Project is using gitlab ci and jobs for running unit and e2e tests. 
After successful test jobs project is being deployed to netlify platform. See link above.