import { mount, createLocalVue } from '@vue/test-utils';
import LoginForm from '@/components/LoginForm';
import Vuex from 'vuex';
const localVue = createLocalVue();
localVue.use(Vuex)
describe('LoginForm.vue', () => {
  let actions
  let store
  beforeEach(() => {
    actions = {
      login: jest.fn(),
    }
    store = new Vuex.Store({
      modules: {
        auth: {
          namespaced: true,
          actions
        },
      },
    })


  });
  it('renders 2 inputs and 2 buttons', () => {
    const wrapper = mount(LoginForm, {
      store, localVue
    });
    expect(wrapper.find('[data-test-login-email-input]').exists()).toBe(true)
    expect(wrapper.find('[data-test-login-password-input]').exists()).toBe(true)
    expect(wrapper.find('[data-test-login-button]').exists()).toBe(true)
    expect(wrapper.find('[data-test-redirect-signin-button]').exists()).toBe(true)
  });
  it('emits show-registration-form event when clicks signin button', async () => {
    const wrapper = mount(LoginForm, {
      store, localVue
    });
    await wrapper.find('[data-test-redirect-signin-button]').trigger('click');
    expect(wrapper.emitted('show-registration-form').length).toBe(1);
  })
  it('dispatches auth.login action with correct payload', async ()=>{
    const email = 'test@test.com';
    const password = 123456;
    const wrapper = mount(LoginForm, {
      store, localVue
    });
    await wrapper.setData({email,password});
    await wrapper.find('[data-test-login-button]').trigger('click');
    expect(actions.login).toHaveBeenCalled();
    expect(actions.login.mock.calls[0][1]).toStrictEqual({email,password});
  })
  it('dispatches auth.login action with trimmed email', async ()=>{
    const email = 'test@test.com    ';
    const trimmedEmail = 'test@test.com';
    const password = 123456;
    const wrapper = mount(LoginForm, {
      store, localVue
    });
    const inputForName = wrapper.find('[data-test-login-email-input]');
    inputForName.element.value = email;
    await inputForName.trigger('input');
    await wrapper.setData({password});
    await wrapper.find('[data-test-login-button]').trigger('click');
    expect(actions.login).toHaveBeenCalled();
    expect(actions.login.mock.calls[0][1]).toStrictEqual({email:trimmedEmail,password});
  })
})