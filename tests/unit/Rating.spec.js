import { mount, createLocalVue } from '@vue/test-utils'
import Rating from '@/components/Rating'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faStar } from '@fortawesome/free-solid-svg-icons'
import { faStar as farStar } from '@fortawesome/free-regular-svg-icons'
 
library.add(faStar)
library.add(farStar)
const localVue = createLocalVue()
localVue.component('font-awesome-icon', FontAwesomeIcon);

describe('Rating.vue',()=>{
  it('renders at least some star',()=>{
    const wrapper = mount(Rating,{localVue,propsData:{
      maxStars:1
    }});
    expect(wrapper.findComponent(FontAwesomeIcon).exists()).toBe(true)
  });
  it('renders 5 stars',()=>{
    const wrapper = mount(Rating,{localVue, propsData:{
      maxStars:5
    }});
    expect(wrapper.findAllComponents(FontAwesomeIcon).length).toBe(5)
  });
 })