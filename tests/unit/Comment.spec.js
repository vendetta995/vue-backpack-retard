import { shallowMount, mount, createLocalVue } from '@vue/test-utils'
import Comment from '@/components/Comment'
import Vuex from 'vuex'

const localVue = createLocalVue()
localVue.use(Vuex)

const getItemByIdResponse = {
  "by": "tibbar",
  "descendants": 271,
  "id": 25108800,
  "kids": [25109379, 25109130, 25110397, 25109048, 25109870, 25109051, 25109052, 25115678, 25112987, 25115294, 25109533, 25109979, 25112257, 25111716, 25109441, 25114849, 25110760, 25109598, 25109047, 25111208, 25110435, 25112840, 25109529, 25109155, 25110822, 25109984, 25112882, 25109486, 25109196, 25109527, 25110978, 25115144, 25109732, 25109040, 25109288, 25110609, 25113596, 25112367, 25109277, 25110945],
  "score": 450,
  "status": 'success',
  "time": 1605508685,
  "title": "On Learning Chess as an Adult – From 650 to 1750 in Two Years",
  "type": "story",
  "url": "https://jacobbrazeal.wordpress.com/2020/11/16/on-learning-chess-as-an-adult-from-650-to-1750-in-two-years/"
}
const loadingItem = {
  "id": 25108800,
  status: 'loading'
}

describe('Comment.vue', () => {
  let getters
  let actions
  let hnyaActions
  let store
  beforeEach(() => {
    getters = {
      getItemById: () => () => getItemByIdResponse
    }
    actions = {
      fetchStory: jest.fn(),
      likeItem: jest.fn(),
      repostItem: jest.fn()
    }
    hnyaActions = {
      likeItem: jest.fn(),
      unlikeItem: jest.fn(),
    }
    store = new Vuex.Store({
      modules:{
        item:{
          getters,
          actions
        },
        hnya:{
          namespaced: true,
          getters:{
            userLikedItemsIds:()=>[]
          },
          actions: hnyaActions
        }
      },
    })
  })
  it('renders', () => {
    const wrapper = shallowMount(Comment, {
      store, localVue,
      propsData: {
        commentId: 25108800,
        viewingDiscussion: false
      }
    })
    expect(wrapper.find('.hn-comment-outer').exists()).toBe(true)
    expect(actions.fetchStory).toHaveBeenCalled();
  })
  it('changes button text when button clicked', async () => {
    const wrapper = shallowMount(Comment, {
      store, localVue,
      propsData: {
        commentId: 25108800
      }
    })
    expect(wrapper.find('.hn-discussion-button').text()).toBe('Show discussion');
    await wrapper.find('.hn-discussion-button').trigger('click');
    expect(wrapper.find('.hn-discussion-button').text()).toBe('Hide discussion');
  })
  it('should render spinner if item status eq loading', () => {
    const anotherGetters = {
      getItemById: () => () => loadingItem
    }

    const anotherStore = new Vuex.Store({
      getters: anotherGetters,
      actions
    })
    const wrapper = shallowMount(Comment, {
      store: anotherStore, localVue,
      propsData: {
        commentId: 25108800
      }
    })
    expect(wrapper.find('.lds-dual-ring').exists()).toBe(true);
  })
  it('calls likeItem action when like button clicked',async ()=>{

    const wrapper = shallowMount(Comment, {
      store, localVue,
      propsData: {
        commentId: 25108800
      }
    })
    await wrapper.find('.hn-comment-like').trigger('click');
    expect(hnyaActions.likeItem).toHaveBeenCalled();
  })
  it('toggles discussions section when button clicked', async () => {
    const wrapper = mount(Comment, {
      store, localVue,
      propsData: {
        commentId: 25108800
      }
    })
    expect(wrapper.find('.hn-comment-discussion').exists()).toBe(false);
    await wrapper.find('.hn-discussion-button').trigger('click');
    expect(wrapper.find('.hn-comment-discussion').exists()).toBe(true);
  })
})
