import { mount } from '@vue/test-utils'
import BackpackItems from '@/components/BackpackItems'
import { v4 as uuidv4 } from "uuid";

const socks = {
  title: "Socks",
  id: uuidv4(),
};
const shirt = {
  title: "Shirt",
  id: uuidv4(),
};
describe('BackpackItems.vue', () => {
  it('renders no elements', () => {
    const wrapper = mount(BackpackItems, {
      propsData: {
        items: []
      }
    })
    expect(wrapper.findAll('.item').length).toBe(0)
  })
  it('renders 2 elements', () => {
    const wrapper = mount(BackpackItems, {
      propsData: {
        items: [socks, shirt]
      }
    })
    const items = wrapper.findAll('.item')
    expect(items.length).toBe(2)
  });
  it('renders delete item button', () => {
    const wrapper = mount(BackpackItems, {
      propsData: {
        renderButtons:true,
        items: [socks]
      }
    });

    const deleteButton = wrapper.find(`[data-test-delete-button="${socks.id}"]`);
    expect(deleteButton.exists()).toBe(true);
  });
  it('renders as many buttons as there are items', () => {
    const items = [socks, shirt];
    const wrapper = mount(BackpackItems, {
      propsData: {
        renderButtons:true,
        items
      }
    });

    const deleteButtons = wrapper.findAll(`[data-test-delete-button]`);
    expect(deleteButtons.length).toBe(items.length);
  });
  it('calls deleteItem action once', async () => {
    const items = [socks, shirt];
    const wrapper = mount(BackpackItems, {
      propsData: {
        renderButtons:true,
        items
      }
    });
    const deleteButton = wrapper.find(`[data-test-delete-button="${socks.id}"]`);
    await deleteButton.trigger('click');
    expect(wrapper.emitted()['delete-item']).toBeTruthy()
    //  expect(deleteItem).toHaveBeenCalledTimes(1);
  })
  it('calls deleteItem action once even when clicked twice', async () => {
    const items = [socks, shirt];
    const wrapper = mount(BackpackItems, {
      propsData: {
        renderButtons:true,
        items
      }
    });
    const deleteButton = wrapper.find(`[data-test-delete-button="${socks.id}"]`);
    await deleteButton.trigger('click');
    await deleteButton.trigger('click');
    expect(wrapper.emitted()['delete-item'].length).toBe(1)
  })
  it('renders cross button',()=>{
    const items = [socks];
    const wrapper = mount(BackpackItems, {
      propsData: {
        renderButtons:true,
        items
      }
    });
    const crossOutButton = wrapper.find(`[data-test-cross-button="${socks.id}"]`);
    expect(crossOutButton.exists()).toBe(true)
  })
  it('emits cross-item event when click cross button',async ()=>{
    const items = [socks];
    const wrapper = mount(BackpackItems, {
      propsData: {
        renderButtons:true,
        items
      }
    });
    const crossOutButton = wrapper.find(`[data-test-cross-button="${socks.id}"]`);
    await crossOutButton.trigger('click');
    
    expect(wrapper.emitted()['cross-item'].length).toBe(1)
  })
  it('should not apply crossed-out-item class when item has property crossed === true',()=>{
    const items = [socks];
    const wrapper = mount(BackpackItems, {
      propsData: {
        renderButtons:true,
        items
      }
    });
    const title = wrapper.find('[data-test="item-title"]')    
    expect(title.classes('crossed-out-item')).toBe(false)
  })
  it('applies crossed-out-item class when item has property crossed === true',()=>{
    const items = [Object.assign({},socks,{crossed:true})];
    const wrapper = mount(BackpackItems, {
      propsData: {
        renderButtons:true,
        items
      }
    });
    const title = wrapper.find('[data-test="item-title"]')    
    expect(title.classes('crossed-out-item')).toBe(true)
  })
})
