import ratingCalculator from '@/utils/ratingCalculator';

describe('ratingCalculator',()=>{
  it('should return 5 if ratings are 0,0',()=>{
    expect((ratingCalculator([]))).toBe(0)
  })
  it('should return 4.3 if ratings are 5,4,4',()=>{
    expect((ratingCalculator([5,4,4]))).toBe(4.3)
  })
})