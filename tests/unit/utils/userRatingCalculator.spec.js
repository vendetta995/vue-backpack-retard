import userRatingCalculator from '@/utils/userRatingCalculator';

describe('userRatingCalculator',()=>{
  it('should return 5 if ratings are 0,0',()=>{
    expect((userRatingCalculator([0,0]))).toBe(5)
  })
  it('should return 4.3 if ratings are 5,4,4',()=>{
    expect((userRatingCalculator([5,4,4]))).toBe(4.3)
  })
  it('should return 4.3 if ratings are 5,4,4,0,0,0,0,0',()=>{
    expect((userRatingCalculator([5,4,4,0,0,0,0,0]))).toBe(4.3)
  })
})