import { mount } from '@vue/test-utils'
import BackpackPage from '@/pages/BackpackPage'
import { v4 as uuidv4 } from "uuid";

const socks = {
  title: "Socks",
  id: uuidv4(),
};
const shirt = {
  title: "Shirt",
  id: uuidv4(),
};


//stub getters
//stub actions
//stub mutations
//stub BackpackItems
//stub router

//TODO fix BackpackPage unit tests
describe.skip('BackpackPage.vue', () => {
  it('renders input where I can input new item name', () => {
    const items = [socks, shirt];
    const wrapper = mount(BackpackPage, {
      data() {
        return {
          items
        }
      }
    });
    expect(wrapper.find('[data-test="add-new-item-input"]').exists()).toBe(true)
  });
  it('deletes socks from items', async () => {
    const items = [socks, shirt];
    const wrapper = mount(BackpackPage, {
      data() {
        return {
          items
        }
      }
    });
    const deleteButton = wrapper.find(`[data-test-delete-button="${socks.id}"]`);
    await deleteButton.trigger('click');
    const itemTitles = wrapper.findAll('[data-test="item-title"]');
    expect(itemTitles.length).toBe(1);
  })
 
  it('renders input with necessary placeholder', () => {
    const items = [socks, shirt];
    const wrapper = mount(BackpackPage, {
      data() {
        return {
          items
        }
      }
    });
    expect(wrapper.find('[data-test="add-new-item-input"]').attributes('placeholder')).toBe('Type item name here');
  });
  it('renders add new item button', () => {
    const items = [socks, shirt];
    const wrapper = mount(BackpackPage, {
      data() {
        return {
          items
        }
      }
    });
    const addNewButton = wrapper.find('[data-test="add-new-item-button"]');
    expect(addNewButton.exists()).toBe(true);
    expect(addNewButton.text()).toBe('Add item');
  });
  it('adds new item = Boots to the list', async () => {
    const newItem ='Boots';
    const items = [];
    const wrapper = mount(BackpackPage, {
      data() {
        return {
          items
        }
      }
    });
    const inputForName = wrapper.find('[data-test="add-new-item-input"]');
    inputForName.element.value = newItem;
    inputForName.trigger('input')
    const addNewButton = wrapper.find('[data-test="add-new-item-button"]');
    await addNewButton.trigger('click');
    expect(wrapper.find('[data-test="item-title"]').text()).toBe(newItem)
  });
  it('adds new item = Boots to the bottom of the list', async () => {
    const newItem ='Boots';
    const items = [shirt,socks];
    const wrapper = mount(BackpackPage, {
      data() {
        return {
          items
        }
      }
    });
    const inputForName = wrapper.find('[data-test="add-new-item-input"]');
    inputForName.element.value = newItem;
    inputForName.trigger('input')
    const addNewButton = wrapper.find('[data-test="add-new-item-button"]');
    await addNewButton.trigger('click');
    expect(wrapper.findAll('[data-test="item-title"]').at(2).text()).toBe(newItem)
  });
  it('applies crossed-out-item when click cross-button', async ()=>{
    const items = [socks,shirt];
    const wrapper = mount(BackpackPage, {
      data() {
        return {
          items
        }
      }
    });
    const crossButton = wrapper.find(`[data-test-cross-button="${socks.id}"]`);
    await crossButton.trigger('click'); 
    expect(wrapper.findAll('[data-test="item-title"]').at(0).classes('crossed-out-item')).toBe(true)
  })
  it('dissolves crossed-out-item class when click cross-button', async ()=>{
    const items = [Object.assign({},socks,{crossed:true}),shirt];
    const wrapper = mount(BackpackPage, {
      data() {
        return {
          items
        }
      }
    });
    const crossButton = wrapper.find(`[data-test-cross-button="${socks.id}"]`);
    expect(wrapper.findAll('[data-test="item-title"]').at(0).classes('crossed-out-item')).toBe(true)
    await crossButton.trigger('click'); 
    expect(wrapper.findAll('[data-test="item-title"]').at(0).classes('crossed-out-item')).toBe(false)
  });
})