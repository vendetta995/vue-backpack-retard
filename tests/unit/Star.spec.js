
import {
  mount,
  createLocalVue
} from '@vue/test-utils'
import Star from '@/components/Star'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faStar } from '@fortawesome/free-solid-svg-icons'
import { faStar as farStar } from '@fortawesome/free-regular-svg-icons'

library.add(faStar)
library.add(farStar)
const localVue = createLocalVue()
localVue.component('font-awesome-icon', FontAwesomeIcon);

describe('Star.vue', () => {
  it('emits set-rating event', async () => {
    debugger;
    const wrapper = mount(Star, { localVue });
    await wrapper.find('.star').trigger('click');
    expect(wrapper.emitted()['set-rating'].length).toBe(1);
  });
  it('data-prefix should be fas when rating is equal to starId',()=>{
    const wrapper = mount(Star, {
      localVue, propsData: {
        starId: 1,
        rating: 1
      }
    });
    expect(wrapper.find('.star').attributes('data-prefix')).toBe('fas');
  });
  it('data-prefix should be far when rating is less than starId',()=>{
    const wrapper = mount(Star, {
      localVue, propsData: {
        starId: 1,
        rating: 0
      }
    });
    expect(wrapper.find('.star').attributes('data-prefix')).toBe('far');
  });
  it('should not apply star-active specific style if rating is more than id', () => {
    const wrapper = mount(Star, {
      localVue, propsData: {
        starId: 1,
        rating: 0
      }
    });
    expect(wrapper.find('.star').classes('star-active')).toBe(false)
  });
  it('should apply star-active specific style if rating is equal to starId', () => {
    const wrapper = mount(Star, {
      localVue, propsData: {
        starId: 1,
        rating: 1
      }
    });
    expect(wrapper.find('.star').classes('star-active')).toBe(true)
  });
  it('should apply star-active specific style if rating is greater than starId', () => {
    const wrapper = mount(Star, {
      localVue, propsData: {
        starId: 1,
        rating: 2
      }
    });
    expect(wrapper.find('.star').classes('star-active')).toBe(true)
  });
  it.todo('should turn #ffe100 when hovered and rating is less than id');
  it.todo('specific style when hovered and rating is less than id')
  it.todo('fills in precisely as many stars as initialRating')
})