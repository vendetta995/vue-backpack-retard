import { mount } from '@vue/test-utils'
import CommentComponent from '@/components/CommentComponent'

describe('CommentComponent.vue', () => {
  it('renders by, time and text when passed', () => {
    const wrapper = mount(CommentComponent, {
      propsData: {
        comment: {
          by: 'me',
          time: 123123,
          text: 'kekus maximus'
        }
      }
    })
    expect(wrapper.text()).toContain('kekus maximus')
  })
})
