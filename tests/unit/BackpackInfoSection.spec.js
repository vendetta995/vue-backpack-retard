import BackpackInfoSection from '../../src/components/BackpackInfoSection';
import { mount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faStar } from '@fortawesome/free-solid-svg-icons'
import { faStar as farStar } from '@fortawesome/free-regular-svg-icons'

library.add(faStar)
library.add(farStar)

const localVue = createLocalVue()
localVue.component('font-awesome-icon', FontAwesomeIcon);
localVue.use(Vuex)


describe('BackpackInfoSection', () => {
  describe('with non-rated backpack', () => {
    let actions
    let getters
    let store
    beforeEach(() => {
      getters = {
        currentBackpack: () => {
          return {
            title: "Sample title",
            creatorId: 1,
            creatorName: 'Sample name',
            items: [],
            ratings: {},
            description: "Sample description"
          };
        }
      }
      actions = {
        rateBackpack: jest.fn(),
      }
      store = new Vuex.Store({
        modules: {
          backpacks: {
            namespaced: true,
            actions,
            getters
          }
        }
      })
    })
    it('should not apply .star-active class to any of the stars', async () => {
      const wrapper = mount(BackpackInfoSection, {
        store, localVue
      })
      await wrapper.setData({
        auth: {
          currentUser: {
            uid: 2
          }
        }
      });
      expect(wrapper.findAll('.star-active').length).toBe(0);
    })
  })
  describe('with rated backpack',()=>{
    let actions
    let getters
    let store
    beforeEach(() => {
      getters = {
        currentBackpack: () => {
          return {
            title: "Sample title",
            creatorId: 1,
            creatorName: 'Sample name',
            items: [],
            ratings: {
              2: 4
            },
            description: "Sample description"
          };
        }
      }
      actions = {
        rateBackpack: jest.fn(),
      }
      store = new Vuex.Store({
        modules: {
          backpacks: {
            namespaced: true,
            actions,
            getters
          }
        }
      })
    })
    it('renders', () => {
      const wrapper = mount(BackpackInfoSection, {
        store, localVue
      })
  
      expect(wrapper.find('[data-test-backpack-info-section-wrapper]').exists()).toBe(true)
    })
    describe('for Non-owner', () => {
      let wrapper
      beforeEach(async () => {
        wrapper = mount(BackpackInfoSection, {
          store, localVue
        })
        await wrapper.setData({
          auth: {
            currentUser: {
              uid: 2
            }
          }
        });
      })
      it('should render rating section', () => {
        expect(wrapper.find('[data-test-rating]').exists()).toBe(true)
      })
      it('should render non-owner section', () => {
        expect(wrapper.find('[data-test-info-section-non-owner]').exists()).toBe(true)
        expect(wrapper.find('[data-test-info-section-owner]').exists()).toBe(false)
      })
    })
    describe('for Owner', () => {
      let wrapper
      beforeEach(async () => {
        wrapper = mount(BackpackInfoSection, {
          store, localVue
        })
        await wrapper.setData({
          auth: {
            currentUser: {
              uid: 1
            }
          }
        });
      })
      it('should not render rating section', () => {
        expect(wrapper.find('[data-test-rating]').exists()).toBe(false)
      })
      it('should render owner section', () => {
        expect(wrapper.find('[data-test-info-section-owner]').exists()).toBe(true)
        expect(wrapper.find('[data-test-info-section-non-owner]').exists()).toBe(false)
      })
    })
    it('renders as many stars filled as rating says', async () => {
      const wrapper = mount(BackpackInfoSection, {
        store, localVue
      })
      await wrapper.setData({
        auth: {
          currentUser: {
            uid: 2
          }
        }
      });
      expect(wrapper.findAll('.star-active').length).toBe(4);
    })
    it('should render rating to person who owns backpack', async () => {
      const wrapper = mount(BackpackInfoSection, {
        store, localVue
      })
      await wrapper.setData({
        auth: {
          currentUser: {
            uid: 1
          }
        }
      });
      expect(wrapper.find('[data-test="actual-score"]').text()).toContain("4")
    })
    it('should render rating to person who doesnt own backpack', async () => {
      const wrapper = mount(BackpackInfoSection, {
        store, localVue
      })
      await wrapper.setData({
        auth: {
          currentUser: {
            uid: 2
          }
        }
      });
      expect(wrapper.find('[data-test="actual-score"]').text()).toContain("4")
    })
  
    it('calls rate backpack action when clicking any star', async () => {
      const wrapper = mount(BackpackInfoSection, {
        store, localVue
      })
      await wrapper.setData({
        auth: {
          currentUser: {
            uid: 2
          }
        }
      });
      await wrapper.find('.star').trigger('click')
      expect(actions.rateBackpack).toHaveBeenCalled()
    })
  })
})