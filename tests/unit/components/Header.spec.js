import { mount, createLocalVue } from '@vue/test-utils';
import Header from '@/components/Header';
import VueRouter from 'vue-router'
import Vuex from 'vuex';
const localVue = createLocalVue()
localVue.use(VueRouter)
localVue.use(Vuex)
const router = new VueRouter({
  routes: [
    {
      path: '/'
    },
    {
      path: '/login',
      name: 'login',
    },
    {
      path: '/all-backpacks',
    },
    {
      path: '/my-backpacks',
    },
  ]
})
describe('Header.vue', () => {
  let actions
  let getters
  let store
  beforeEach(() => {
    getters = {
      userProfile: () => {
        return {}
      }
    }
    actions = {
      logoutUser: jest.fn(),
    }
    store = new Vuex.Store({
      modules: {
        auth: {
          namespaced: true,
          actions,
          getters
        }
      }
    })
  })
  it('renders', () => {
    const wrapper = mount(Header, {
      localVue, router, store
    })
    
    expect(wrapper.find('.hn-header').exists()).toBe(true);
  })
  it('should apply hn-header-main-page when showed on main page', async () => {
    const wrapper = mount(Header, {
      localVue, router, store
    })
    expect(wrapper.find('.hn-header-main-page').exists()).toBe(true);
  })
  it('should not apply hn-header-main-page class when showed on other page', async () => {
    const wrapper = mount(Header, {
      localVue, router, store
    })
    await router.push('/login')
    expect(wrapper.find('.hn-header-main-page').exists()).toBe(false);
  })

  describe('When name is presented in userProfile', () => {
    let wrapper;
    let gettersWithUserInfo;
    let storeWithUserInfo;
    beforeEach(async () => {
      gettersWithUserInfo = {
        userProfile: () => {
          return { name: 'Sample name', rating: 5 }
        }
      }
      storeWithUserInfo = new Vuex.Store({
        modules: {
          auth: {
            namespaced: true,
            actions,
            getters: gettersWithUserInfo
          }
        }
      })
      wrapper = mount(Header, {
        localVue, router, store: storeWithUserInfo
      })
      await wrapper.setData({
        auth: {
          currentUser: true
        },
        isMobile:false
      })
    })
    it('renders [data-test-user-header]', () => {
      expect(wrapper.find('[data-test-user-header]').exists()).toBe(true);
    })
    it('renders main page button in header', () => {
      expect(wrapper.find('[data-test-main-page-link]').exists()).toBe(true);
    });
    it('renders all-backpacks link in header', () => {
      expect(wrapper.find('[data-test-all-backpacks-link]').exists()).toBe(true);
    });
    it('renders my-backpacks link in header', () => {
      expect(wrapper.find('[data-test-my-backpacks-link]').exists()).toBe(true);
    });

    it('renders username', () => {
      expect(wrapper.find('[data-test-header-username]').exists()).toBe(true);
      expect(wrapper.find('[data-test-header-username]').text()).toBe('Sample name');
    })
    it('renders rating', () => {
      expect(wrapper.find('[data-test-header-rating]').exists()).toBe(true);
      expect(wrapper.find('[data-test-header-rating]').text()).toBe("5");
    })
    it('logoutUser is being called', async () => {
      await wrapper.find('[data-test-logout-button]').trigger('click')
      expect(actions.logoutUser).toHaveBeenCalled();
    })
  })
  describe('When no name in userProfile', () => {
    let wrapper;
    beforeEach(async () => {
      wrapper = mount(Header, {
        localVue, router, store
      })
      await wrapper.setData({
        auth: {
          currentUser: false
        },
        isMobile:false
      })
    })
    it('renders data-test-stranger-header', () => {
      expect(wrapper.find('[data-test-stranger-header]').exists()).toBe(true);
    })
    it('renders main page button in header', () => {
      expect(wrapper.find('[data-test-main-page-link]').exists()).toBe(true);
    });
    it('renders all-backpacks link in header', () => {
      expect(wrapper.find('[data-test-all-backpacks-link]').exists()).toBe(true);
    });
    it('does not render all-backpacks link in header', () => {
      expect(wrapper.find('[data-test-my-backpacks-link]').exists()).toBe(false);
    });
    it('does not render username', () => {
      expect(wrapper.find('[data-test-header-username]').exists()).toBe(false);
    })
    it('does not render logoutUser button', () => {
      expect(wrapper.find('[data-test-logout-button]').exists()).toBe(false);
    })
    it('renders login link', () => {
      expect(wrapper.find('[data-test-login-link]').exists()).toBe(true);
    })
  })
})