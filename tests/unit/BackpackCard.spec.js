import BackpackCard from '@/components/BackpackCard'
import { mount, createLocalVue } from '@vue/test-utils'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faChevronRight } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import VueRouter from 'vue-router'
library.add(faChevronRight)
const localVue = createLocalVue()
localVue.use(VueRouter)
localVue.component('font-awesome-icon', FontAwesomeIcon);
const router = new VueRouter({
  routes: [
    {
      path: '/backpack/:id',
      name: 'backpack'
    }
  ]
})

describe('BackpackCard', () => {
  describe('basics', () => {
    let wrapper;
    let backpack;
    beforeEach(() => {
      backpack = {
        id: 1,
        title: 'sampleText',
        creatorId: 1,
        creatorName: 'Sample name',
        items: [],
        description: 'Sample description'
      };
      wrapper = mount(BackpackCard, {
        localVue, router, propsData: {
          auth: {
            currentUser: {
              uid: 1
            }
          },
          backpack
        }
      })
    })
    describe('renders correct meta info', () => {
      it('renders rating', () => {
        expect(wrapper.find(`[data-test-backpack-rating-number="1"]`).exists()).toBe(true);
      })
      it('renders title', () => {
        expect(wrapper.find(`[data-test-backpack-title="${backpack.id}"]`).exists()).toBe(true);
        expect(wrapper.find(`[data-test-backpack-title="${backpack.id}"]`).text()).toBe(backpack.title);
      })
      it('renders description', () => {
        expect(wrapper.find(`[data-test-backpack-description="${backpack.id}"]`).exists()).toBe(true);
        expect(wrapper.find(`[data-test-backpack-description="${backpack.id}"]`).text()).toBe(backpack.description);
      })
    })
    describe('renders correct meta info', () => {
      it('renders id', () => {
        expect(wrapper.find(`[data-test-backpack-id="${backpack.id}"]`).exists()).toBe(true);
        expect(wrapper.find(`[data-test-backpack-id="${backpack.id}"]`).text()).toBe(`#${backpack.id}`);
      })
      it('renders author', () => {
        expect(wrapper.find(`[data-test-backpack-creator="${backpack.id}"]`).exists()).toBe(true);
        expect(wrapper.find(`[data-test-backpack-creator="${backpack.id}"]`).text()).toBe(`Made by ${backpack.creatorName}`);
      })
    })
  })
  describe('button group cases', () => {
    let wrapper;
    let backpack;
    beforeEach(() => {
      backpack = {
        id: 1,
        title: 'sampleText',
        creatorId: 1,
        creatorName: 'Sample name',
        items: [],
        description: 'Sample description'
      };
      wrapper = mount(BackpackCard, {
        localVue, router, propsData: {
          backpack
        }
      })

    })
    it('should render EDIT ME button', async () => {
      await wrapper.setData({
        auth: {
          currentUser: {
            uid: 1
          }
        }
      });
      expect(wrapper.find(`[data-test-backpack-link="${backpack.id}"]`).exists()).toBe(true);
      expect(wrapper.find(`[data-test-backpack-link="${backpack.id}"]`).text()).toBe('EDIT ME');
    })
    it('should render OPEN ME button', async () => {
      await wrapper.setData({
        auth: {
          currentUser: {
            uid: 2
          }
        }
      });
      expect(wrapper.find(`[data-test-backpack-link="${backpack.id}"]`).exists()).toBe(true);
      expect(wrapper.find(`[data-test-backpack-link="${backpack.id}"]`).text()).toBe('OPEN ME');
    })
    it('should render Login to see items button', async () => {
      await wrapper.setData({
        auth: {
          currentUser: null
        }
      });
      expect(wrapper.find(`[data-test-login-backpack-link="${backpack.id}"]`).exists()).toBe(true);
      expect(wrapper.find(`[data-test-login-backpack-link="${backpack.id}"]`).text()).toBe('Login to see items');
    })
  })

  describe(`rating's display section`, () => {
    describe('with no ratings', () => {
      let wrapper;
      let backpack;
      beforeEach(() => {
        backpack = {
          id: 1,
          title: 'sampleText',
          creatorId: 1,
          creatorName: 'Sample name',
          items: [],
          description: 'Sample description'
        };
        wrapper = mount(BackpackCard, {
          localVue, router, propsData: {
            auth: {
              currentUser: {
                uid: 1
              }
            },
            backpack
          }
        })
      })

      it('renders rating when no ratings were set', () => {
        expect(wrapper.find(`[data-test-backpack-rating-number="1"]`).text()).toContain("Not rated yet");
      })
    })
    describe('with ratings', () => {
      it('renders 4.5 when ratings were set', () => {
        const backpack = {
          id: 1,
          title: 'sampleText',
          creatorId: 1,
          creatorName: 'Sample name',
          items: [],
          description: 'Sample description',
          ratings: {
            1: 5,
            2: 4,
            3: 4,
            4: 5
          }
        };
        const wrapper = mount(BackpackCard, {
          localVue, router, propsData: {
            auth: {
              currentUser: {
                uid: 1
              }
            },
            backpack
          }
        })

        expect(wrapper.find(`[data-test-backpack-rating-number="1"]`).text()).toEqual('4.5');

      })
      it('renders 4.3 when ratings were set', () => {
        const backpack = {
          id: 1,
          title: 'sampleText',
          creatorId: 1,
          creatorName: 'Sample name',
          items: [],
          description: 'Sample description',
          ratings: {
            1: 5,
            2: 4,
            3: 4
          }
        };
        const wrapper = mount(BackpackCard, {
          localVue, router, propsData: {
            auth: {
              currentUser: {
                uid: 1
              }
            },
            backpack
          }
        })

        expect(wrapper.find(`[data-test-backpack-rating-number="1"]`).text()).toEqual('4.3');

      })
    })
  })
})