import faker from 'faker';
describe.skip('Certain backpack page', () => {
  describe('Stranger', () => { })
  describe('User', () => {
    before(() => {
      cy.login('daniel@test.com', 123456);
      // cy.contains('My Backpacks').click();
      // cy.url().should('contain', '/my-backpacks');
      // cy.contains('Add backpack').click();
      // cy.get('[data-test-backpack-name-input]').type('My awesome backpack');
      // cy.contains('Create new backpack').click();

    })
    beforeEach(() => {
      cy.visit('/backpack/8gf7KUP3FN6aKV6qY10p');
      cy.url().should('contain', '/backpack');
    })
    describe('Buttons interactions', () => {
      it('"Save backpack" should become disabled and contain spinner after click', () => {
        cy.get('[data-test="save-backpack-button"]').click();
        cy.get('[data-test="save-backpack-button"]').should('be.disabled');
        cy.get('[data-test="save-backpack-button"]').find('.loader').should('be.visible');
      })
    })
    describe('Editing backpack properties', () => {
      it('can change title and save title', () => {
        const title = faker.random.words();
        cy.get('[data-test="backpack-title"]').clear().type(title);
        cy.contains('Save backpack').click();
        cy.get('[data-test="save-backpack-button"]').should('be.disabled');
        cy.get('[data-test="save-backpack-button"]').find('.loader').should('be.visible');
        cy.get('[data-test="save-backpack-button"]').should('not.be.disabled');
        cy.visit('/backpack/8gf7KUP3FN6aKV6qY10p');
        cy.url().should('contain', '/backpack');
        cy.get('[data-test="backpack-title"]').should('have.value', title);
      })
      it('can change occasion and save occasion', () => {
        const title = faker.random.words();
        cy.get('[data-test="backpack-occasion"]').clear().type(title);
        cy.contains('Save backpack').click();
        cy.get('[data-test="save-backpack-button"]').should('be.disabled');
        cy.get('[data-test="save-backpack-button"]').find('.loader').should('be.visible');
        cy.get('[data-test="save-backpack-button"]').should('not.be.disabled');
        
        cy.visit('/');
        cy.visit('/backpack/8gf7KUP3FN6aKV6qY10p');
        cy.url().should('contain', '/backpack');
        cy.get('[data-test="backpack-occasion"]').should('have.value', title);
      })
      it('can change description and save description', () => {
        const title = faker.random.words();
        cy.get('[data-test="backpack-description"]').clear().type(title);
        cy.contains('Save backpack').click();
        cy.get('[data-test="save-backpack-button"]').should('be.disabled');
        cy.get('[data-test="save-backpack-button"]').find('.loader').should('be.visible');
        cy.get('[data-test="save-backpack-button"]').should('not.be.disabled');
        cy.visit('/backpack/8gf7KUP3FN6aKV6qY10p');
        cy.url().should('contain', '/backpack');
        cy.get('[data-test="backpack-description"]').should('have.value', title);
      })
    })
    describe('Adding item into backpack', () => {
      it('renders new item after click', () => {
        const title = faker.random.words();
        cy.get('[data-test="backpack-title"]').should('be.visible')
        cy.get('[data-test="add-new-item-input"]').type(title);
        cy.contains('Add item').click();
        cy.get('.item').contains(title).should('be.visible');
        cy.get('[data-test="add-new-item-input"]').should('have.value', '');
      })
      it('actually saves item into db', () => {
        const title = faker.random.words();
        cy.get('[data-test="backpack-title"]').should('be.visible')
        cy.get('[data-test="add-new-item-input"]').type(title);
        cy.contains('Add item').click();
        cy.contains('Save backpack').click();
        cy.get('[data-test="save-backpack-button"]').should('be.disabled');
        cy.get('[data-test="save-backpack-button"]').find('.loader').should('be.visible');
        cy.get('[data-test="save-backpack-button"]').should('not.be.disabled');
        cy.visit('/backpack/8gf7KUP3FN6aKV6qY10p');
        cy.url().should('contain', '/backpack');
        cy.get('.item').contains(title).should('be.visible');
        // cy.get('.item').contains('Socks').should('be.visible');
        // cy.get('[data-test="add-new-item-input"]').should('have.value', '');
      })
      it.skip('can delete item from backpack', () => {
        const title = faker.random.words();
        cy.get('[data-test="backpack-title"]').should('be.visible')
        cy.get('[data-test="add-new-item-input"]').type(title);
        cy.contains('Add item').click();
        cy.get('[data-test-delete-button]').should('be.visible');
        cy.get('[data-test-delete-button]').click();
        cy.contains(title).should('not.exist');
      })
      it.skip('actually deletes item from db', () => {
        const title = faker.random.words();
        cy.get('[data-test="backpack-title"]').should('be.visible')
        cy.get('[data-test="add-new-item-input"]').type(title);
        cy.contains('Add item').click();
        cy.contains('Save backpack').click();
        cy.get('[data-test="save-backpack-button"]').should('be.disabled');
        cy.get('[data-test="save-backpack-button"]').find('.loader').should('be.visible');
        cy.get('[data-test="save-backpack-button"]').should('not.be.disabled');
        cy.visit('/backpack/8gf7KUP3FN6aKV6qY10p');
        cy.url().should('contain', '/backpack');
        cy.get('.item').contains(title).should('be.visible');
      })
    })
  })
})
