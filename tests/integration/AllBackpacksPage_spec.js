describe('AllBackpacks Page', () => {
  describe('Stranger', () => {
    beforeEach(() => {
      cy.clearSessionStorage();
      cy.visit('/');
      cy.contains('All backpacks').click();
    })
    it('should see any backpack item', () => {
      cy.get('.bp-backpack').should('be.visible');
    })
    it("should see OPEN ME / EDIT ME button, sees login button instead", () => {
      cy.get('[data-test-backpack]').find('[data-test-backpack-link]').should('not.exist');
      cy.get('[data-test-backpack]').find('[data-test-login-backpack-link]').should('exist');
    })
    it("should be redirected to login page after clicking login button", () => {
      // cy.get('[data-test-backpack-link]').should('not.exist');
      cy.get('[data-test-backpack]:first').find('[data-test-login-backpack-link]').click()
      cy.url().should('contain','/login')
    })
  })
  describe("User", () => {
    beforeEach(() => {
      cy.login('daniel@test.com', 123456);
      cy.contains('All backpacks').click();
    })
    it('should be redirected to backpack page after clicking open it button', () => {
      cy.get('[data-test-backpack-link]:first').click();
      cy.url().should('contain', '/backpack');
    })
  })
})