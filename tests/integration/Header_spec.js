  describe('Basics', () => {
    describe('as Stranger', () => {
      it('should see login button and click it', () => {
        cy.visit('/');
        cy.contains('Login').click();
        cy.url('').should('include', '/login');
      })
      it('should stay on main page when click main page link', () => {
        cy.visit('/');
        cy.contains('Main page').click();
        cy.url({ timeout: 10000 }).should('eq', `${Cypress.config().baseUrl}/`);
      })
      it('should see All backpacks link and click it', () => {
        cy.visit('/');
        cy.contains('All backpacks').click();
        cy.url('').should('include', '/all-backpacks');
      })
      it('should not see username ', () => {
        cy.get('[data-test-header-username]').should('not.exist');
      })
      it('should not see link to my-backpacks page', () => {
        cy.contains('My Backpacks').should('not.exist');
      })
    })
    describe('as User', () => {
      beforeEach(() => {
        cy.login('daniel@test.com', 123456);
      })
      afterEach(() => {
        cy.clearSessionStorage();
      })
      describe('Main page header', () => {
        it('should apply necessary classes', () => {
          cy.get('[data-test-header-username]').should(div => {
            expect(div).to.contain('Daniel')
          })
          cy.get('header').should('have.class', 'hn-header-main-page');
        })
      })
      describe('Non-main page header', () => {
        it('should not apply hn-header-main-page class', () => {
          cy.visit('/');
          cy.contains('All backpacks').click();
          cy.get('header').should('not.have.class', 'hn-header-main-page');
        })
      it('should stay on main page when click main page link', () => {
        cy.visit('/');
        cy.contains('Main page').click();
        cy.url({ timeout: 10000 }).should('eq', `${Cypress.config().baseUrl}/`);
      })
      it('should see All backpacks link and click it', () => {
        cy.visit('/');
        cy.contains('All backpacks').click();
        cy.url('').should('include', '/all-backpacks');
      })
      it('should his username', () => {
        cy.get('[data-test-header-username]').should(div => {
          expect(div).to.contain('Daniel')
        })
      })
      it('should logout user after click logout button', () => {
        cy.contains('Logout').click();
        cy.get('[data-test-header-username]').should('not.exist');
      })
      it('should navigate user to my-backpacks page', () => {
        cy.contains('My Backpacks').click();
        cy.url('').should('include', '/my-backpacks');
      })
    })
  })
})