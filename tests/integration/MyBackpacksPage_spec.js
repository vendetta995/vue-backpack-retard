describe('AllBackpacks Page', () => {
  describe('Stranger', () => {
    it('should not see My backpacks link in header', () => {
      cy.visit('/');
      cy.contains('My Backpacks').should('not.exist');
    })
  })
  describe("User", () => {
    beforeEach(() => {
      cy.clearSessionStorage();
      cy.login('daniel@test.com', 123456);
      cy.contains('My Backpacks').click();
      cy.url().should('contain', '/my-backpacks');
    })
    it('should see "add-backpack" button', () => {
      cy.contains('Add backpack').should('be.visible')
    })
    it(`be redirected to backpack's page after creating new backpack`, () => {
      cy.contains('Add backpack').click();
      cy.get('[data-test-backpack-name-input]').type('My awesome backpack');
      cy.contains('Create new backpack').click();
      cy.url().should('contain', '/backpack');
    })
  })
})