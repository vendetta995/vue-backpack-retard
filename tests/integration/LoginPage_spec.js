describe.skip('Login Page', () => {
  afterEach(() => {
    cy.clearSessionStorage();
  })

  it('Stranger should be / after successful login', () => {
    const userName = 'daniel@test.com';
    const password = '123456';
    cy.visit('/login')
    cy.get('[data-test-login-email-input]').type(userName)
    cy.get('[data-test-login-password-input]').type(password)
    cy.get('[data-test-login-button]').click()
    cy.url({ timeout: 10000 }).should('eq', `${Cypress.config().baseUrl}/`);
  })
})