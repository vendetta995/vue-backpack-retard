describe('Main Page', () => {
  describe('Stranger', () => {
    it('should not be redirected to /login when visits homepage', () => {
      cy.visit('/')
      cy.url({ timeout: 10000 }).should('eq', `${Cypress.config().baseUrl}/`);
    })
    it('should be able to see inspirational quotes', () => {
      cy.get('.first-quote', { timeout: 10000 }).should('contain', `You miss 100% of the shots you don't take. - Wayne Gretzky`);
      cy.get('.last-quote', { timeout: 10000 }).should('contain', `"Never, ever, ever, give up" - Michael Scott`);
      cy.get('.inspirational-quote', { timeout: 10000 }).should('contain', `Would I rather be feared or loved?`);
    });
    it('should see man on a faggio and a button', () => {
      cy.visit('')
      cy.scrollTo('bottom')
      cy.get('.faggio-man').should('exist')
      cy.get('[data-test-backpacking-link]').click();
      cy.url().should('contain', '/all-backpacks');
    })
  })
})